import { createContext, useState, useEffect } from "react";
import { api } from "../api";
import useRefreshToken from "../hooks/useRefreshToken";

const AuthContext = createContext({});

export const AuthProvider = ({ children }) => {
   const [auth, setAuth] = useState({});
   const [init, setInit] = useState(true);
   const refresh = async () => {
      try {
         const { data } = await api.get("/refresh", {
            withCredentials: true,
         });
         setAuth((prev) => {
            return { ...prev, accessToken: data.AccTkn, codusu: data.CodUsu };
         });
      } catch (error) {
      } finally {
         setInit(false);
      }
      // return response.data;
   };

   useEffect(() => {
      refresh();
   }, []);

   return <AuthContext.Provider value={{ auth, setAuth, init }}>{children}</AuthContext.Provider>;
};

export default AuthContext;
