import { useNavigate } from "react-router-dom";

const Unauthorized = () => {
   const navigate = useNavigate();
   const goBack = () => navigate(-1);
   alert("Unauthorized");
   return goBack;
};

export default Unauthorized;
