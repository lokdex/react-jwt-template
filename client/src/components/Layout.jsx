import useAuth from "../hooks/useAuth";
import { api } from "../api";
import { Outlet } from "react-router-dom";

import { Box, Flex, Image, Button, Spacer } from "@chakra-ui/react";
import Imagotipo from "../images/ucsm-imagotipo-blanco.svg";

const Layout = () => {
   const { setAuth, auth } = useAuth();
   const handleLogout = async () => {
      try {
         let { data } = await api.post("/logout");
         setAuth({});
      } catch (error) {
         console.log(error);
      }
   };
   return (
      <Box height="100%" width="100%">
         <Box bg="brand" p="0.5rem" height="7vh">
            <Flex alignItems="center">
               <Image src={Imagotipo} h="3rem" />
               <Spacer />
               {auth.codusu && (
                  <>
                     <Button variant="ghost" _hover={{ bgColor: "#06A95F" }} color="white">
                        Menú
                     </Button>
                     <Button variant="ghost" _hover={{ bgColor: "#06A95F" }} color="white" onClick={handleLogout}>
                        Cerrar Sesión
                     </Button>
                  </>
               )}
            </Flex>
         </Box>
         <Outlet />
         <Box textAlign="center" height="3vh">
            © {new Date().getFullYear()} Universidad Católica de Santa María. Todos los derechos reservados.
         </Box>
      </Box>
   );
};

export default Layout;
