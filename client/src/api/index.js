import axios from "axios";
const BASE_URL = "http://localhost:8080/index.php";

export const api = axios.create({
   baseURL: BASE_URL,
   withCredentials: true,
});
