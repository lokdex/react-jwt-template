import { useState } from "react";
import { Routes, Route } from "react-router-dom";
import Layout from "./components/Layout";
import RequireAuth from "./components/RequireAuth";
import useAuth from "./hooks/useAuth";
import Menu from "./pages/Menu";
import Login from "./pages/Login";

import "./App.css";

function App() {
   const { init } = useAuth();
   return init ? (
      <></>
   ) : (
      <Routes>
         <Route path="/" element={<Layout />}>
            {/* Public routes */}
            <Route path="login" element={<Login />} />

            {/* Private routes */}
            <Route element={<RequireAuth />}>
               <Route path="/" element={<Menu />} />
            </Route>

            {/* 404 */}
            {/* <Route path="*" element={<NotFound />} /> */}
         </Route>
      </Routes>
   );
}

export default App;
