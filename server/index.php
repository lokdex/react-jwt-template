<?php
header("Access-Control-Allow-Origin: http://localhost:3000");
header(
   "Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization, X-Requested-With"
);
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
header("Allow: GET, POST, OPTIONS");
header("Content-type: application/json");
$method = $_SERVER["REQUEST_METHOD"];
if ($method == "OPTIONS") {
   die();
}

require_once "Clases/CLogin.php";

// Obtener URI
$uri = parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH);
$uri = array_slice(explode("/", $uri), 2);
$REQUEST = file_get_contents("php://input");
$DATA = json_decode($REQUEST, true);

switch ($uri[0]) {
   case "login":
      $oLogin = new CLogin();
      $oLogin->paData = $DATA;
      $llOk = $oLogin->omIniciarSesion();
      if (!$llOk) {
         echo json_encode($oLogin->pcError);
         http_response_code(400);
      }
      echo json_encode($oLogin->paDatos);
      break;
   case "validate":
      $oLogin = new CLogin();
      $llOk = $oLogin->omValidateToken();
      if (!$llOk) {
         http_response_code(403);
      }
      break;
   case "refresh":
      $oLogin = new CLogin();
      $llOk = $oLogin->omRefreshToken();
      if (!$llOk) {
         http_response_code(403);
      }
      echo json_encode($oLogin->paDatos);
      break;
   case "initMenu":
      $oLogin = new CLogin();
      $llOk = $oLogin->omValidateToken();
      if (!$llOk) {
         http_response_code(403);
      } else {
         $arr = [
            "OPC" => "Value",
         ];
         echo json_encode($arr);
      }
      break;
   default:
      http_response_code(404);
}

?>
