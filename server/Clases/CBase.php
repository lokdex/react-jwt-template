<?php

//------------------------------------------------------
// Clase Base
//------------------------------------------------------
class CBase {
   public $pcError;

   function __construct() {
      $this->pcError = null;
   }
}

//------------------------------------------------------
// Clase para fechas
//------------------------------------------------------
class CDate extends CBase {
   public $date;
   public $days;

   public function valDate($p_dFecha) {
      $laFecha = explode("-", $p_dFecha);
      $llOk = checkdate(
         (int) $laFecha[1],
         (int) $laFecha[2],
         (int) $laFecha[0]
      );
      if (!$llOk) {
         $this->pcError = "FORMATO DE FECHA INVALIDA";
      }
      return $llOk;
   }

   public function add($p_dFecha, $p_nDias) {
      $llOk = $this->valDate($p_dFecha);
      if (!$llOk) {
         return false;
      }
      if (!is_int($p_nDias)) {
         $this->pcError = "PARAMETRO DE DIAS ES INVALIDO";
         return false;
      } elseif ($p_nDias >= 0) {
         $lcDias = " + " . $p_nDias . " days";
      } else {
         $p_nDias = $p_nDias * -1;
         $lcDias = " - " . $p_nDias . " days";
      }
      $this->date = date("Y-m-d", strtotime($p_dFecha . $lcDias));
      return true;
   }

   public function diff($p_dFecha1, $p_dFecha2) {
      $llOk = $this->valDate($p_dFecha1);
      if (!$llOk) {
         return false;
      }
      $llOk = $this->valDate($p_dFecha2);
      if (!$llOk) {
         return false;
      }
      $this->days = (strtotime($p_dFecha1) - strtotime($p_dFecha2)) / 86400;
      $this->days = floor($this->days);
      return true;
   }

   public function dateText($p_dDate) {
      $llOk = $this->valDate($p_dDate);
      if (!$llOk) {
         return "Error: " . $p_dDate;
      }
      $laDays = [
         "Domingo",
         "Lunes",
         "Martes",
         "Miércoles",
         "Jueves",
         "Viernes",
         "Sábado",
      ];
      $laMonths = [
         "Enero",
         "Febrero",
         "Marzo",
         "Abril",
         "Mayo",
         "Junio",
         "Julio",
         "Agosto",
         "Septiembre",
         "Octubre",
         "Noviembre",
         "Diciembre",
      ];
      $laDate = explode("-", $p_dDate);
      $ldDate = mktime(0, 0, 0, $laDate[1], $laDate[2], $laDate[0]);
      return $laDays[date("w", $ldDate)] .
         ", " .
         date("d", $ldDate) .
         " " .
         $laMonths[date("m", $ldDate) - 1] .
         " de " .
         date("Y", $ldDate);
   }

   public function mxvalDate($p_dFecha) {
      $laFecha = explode("-", $p_dFecha);
      $llOk = checkdate(
         (int) $laFecha[1],
         (int) $laFecha[2],
         (int) $laFecha[0]
      );
      if (!$llOk) {
         $this->pcError = "FORMATO DE FECHA INVALIDA";
      }
      return $llOk;
   }

   public function dateTextMonth($p_dDate) {
      if (strlen($p_dDate) != 2) {
         return "Error: " . $p_dDate;
      }
      $p_dDate = (int) $p_dDate;
      $laMonths = [
         "Enero",
         "Febrero",
         "Marzo",
         "Abril",
         "Mayo",
         "Junio",
         "Julio",
         "Agosto",
         "Septiembre",
         "Octubre",
         "Noviembre",
         "Diciembre",
      ];
      return $laMonths[$p_dDate - 1];
   }

   public function dateSimpleText($p_dDate) {
      $llOk = $this->valDate($p_dDate);
      if (!$llOk) {
         return "Error: " . $p_dDate;
      }
      $laMonths = [
         "Enero",
         "Febrero",
         "Marzo",
         "Abril",
         "Mayo",
         "Junio",
         "Julio",
         "Agosto",
         "Septiembre",
         "Octubre",
         "Noviembre",
         "Diciembre",
      ];
      $laDate = explode("-", $p_dDate);
      $ldDate = mktime(0, 0, 0, $laDate[1], $laDate[2], $laDate[0]);
      return date("d", $ldDate) .
         " de " .
         $laMonths[date("m", $ldDate) - 1] .
         " del " .
         date("Y", $ldDate);
   }

   public function omMonth($p_cMonth) {
      $lnMonth = (int) $p_cMonth;
      $laMonths = [
         "Enero",
         "Febrero",
         "Marzo",
         "Abril",
         "Mayo",
         "Junio",
         "Julio",
         "Agosto",
         "Septiembre",
         "Octubre",
         "Noviembre",
         "Diciembre",
      ];
      return $laMonths[$lnMonth - 1];
   }

   public function formatDate($p_dDate) {
      $llOk = $this->valDate($p_dDate);
      if (!$llOk) {
         return "ERR:" . $p_dDate;
      }
      $laMonths = [
         "ENE",
         "FEB",
         "MAR",
         "ABR",
         "MAY",
         "JUN",
         "JUL",
         "AGO",
         "SET",
         "OCT",
         "NOV",
         "DIC",
      ];
      $i = intval(substr($p_dDate, 5, 2)) - 1;
      $ldDate = substr($p_dDate, 0, 5) . $laMonths[$i] . substr($p_dDate, 7, 3);
      return $ldDate;
   }
}

function fxAlert($p_Message) {
   echo "<script type=\"text/javascript\">";
   echo "alert('$p_Message')";
   echo "</script>";
}

function fxHeader($p_cLocation, $p_cMensaje = "") {
   if (empty($p_cMensaje)) {
      $lcScript = "window.location='$p_cLocation';";
   } else {
      $lcScript = "alert('$p_cMensaje');window.location='$p_cLocation';";
      //$lcScript = "window.location='$p_cLocation';alert('$p_cMensaje');";
   }
   echo "<script>" . $lcScript . "</script>";
}

function right($lcCadena, $count) {
   return substr($lcCadena, $count * -1);
}

function left($lcCadena, $count) {
   return substr($lcCadena, 0, $count);
}

function fxNumber($p_nNumero, $p_nLength, $p_nDecimal) {
   $lcNumero = number_format($p_nNumero, $p_nDecimal, ".", ",");
   $lcCadena = str_repeat(" ", $p_nLength) . $lcNumero;
   return right($lcCadena, $p_nLength);
}

function fxString($p_cCadena, $p_nLength) {
   #$i = substr_count($p_cCadena, 'Ñ');
   $lcCadena = $p_cCadena . str_repeat(" ", $p_nLength);
   #$lcCadena = substr($lcCadena, 0, $p_nLength + $i);
   $lcCadena = substr($lcCadena, 0, $p_nLength);
   return $lcCadena;
}

function fxStringf($p_cCadena, $p_nLength) {
   $i = substr_count($p_cCadena, "Ñ");
   $lcCadena = $p_cCadena . str_repeat(" ", $p_nLength);
   $lcCadena = substr($lcCadena, 0, $p_nLength + $i);
   return $lcCadena;
}

function fxInitSession() {
   if (
      !(
         isset($_SESSION["GCNOMBRE"]) and
         isset($_SESSION["GCCODUSU"]) and
         isset($_SESSION["GCCENCOS"])
      )
   ) {
      if (isset($_GET["dir"])) {
         $_SESSION["URL"] = $_GET["dir"];
      }
      return false;
   }
   return true;
}

function fxSubstrCount($p_cString) {
   $i = substr_count($p_cString, "Á");
   $i += substr_count($p_cString, "É");
   $i += substr_count($p_cString, "Í");
   $i += substr_count($p_cString, "Ó");
   $i += substr_count($p_cString, "Ú");
   $i += substr_count($p_cString, "Ñ");
   $i += substr_count($p_cString, "ñ");
   $i += substr_count($p_cString, "º");
   $i += substr_count($p_cString, "§");
   $i += substr_count($p_cString, "ø");
   $i += substr_count($p_cString, "á");
   $i += substr_count($p_cString, "é");
   $i += substr_count($p_cString, "í");
   $i += substr_count($p_cString, "ó");
   $i += substr_count($p_cString, "ú");
   $i += substr_count($p_cString, "Ü");
   $i += substr_count($p_cString, "°");
   return $i;
}

function fxStringFixed($p_cString, $p_nLenght) {
   //$lcString = fxString($p_cString, $p_nLenght);
   $i = fxSubstrCount($p_cString);
   $lcString = fxString($p_cString, $p_nLenght + $i);
   return $lcString;
}

function fxStringTail($p_cString, $p_nIndex) {
   $lcString = $p_cString;
   $lcString = substr($lcString, $p_nIndex);
   return $lcString;
}

function fxString2($p_cString, $p_nLenght) {
   $lcString = utf8_decode($p_cString);
   $lcString = $lcString . str_repeat(" ", $p_nLenght);
   $lcString = substr($lcString, 0, $p_nLenght);
   return $lcString;
}

function fxStringTail2($p_cString, $p_nIndex) {
   $lcString = utf8_decode($p_cString);
   $lcString = substr($lcString, $p_nIndex);
   return $lcString;
}

function fxStringCenter($p_cString, $p_nLenght) {
   $lcString = str_pad($p_cString, $p_nLenght, " ", STR_PAD_BOTH);
   $lcString = fxStringFixed($lcString, $p_nLenght);
   return $lcString;
}

function fxDocumento($FilRet) {
   $lcTxt = "<script type=text/javascript>";
   $lcTxt .= "window.open('$FilRet','_blank', 'toolbar=yes, scrollbars=yes, resizable=yes, width=950, height=650');";
   $lcTxt .= "</script>";
   echo $lcTxt;
}

function array_change_key_case_recursive($arr) {
   return array_map(function ($item) {
      if (is_array($item)) {
         $item = array_change_key_case_recursive($item);
      }
      return $item;
   }, array_change_key_case($arr, CASE_UPPER));
}

function validateDate($date, $format = "Y-m-d H:i:s") {
   $d = DateTime::createFromFormat($format, $date);
   return var_dump($d && $d->format($format) == $date);
}

function validateDateTime($dateStr, $format) {
   date_default_timezone_set("UTC");
   $date = DateTime::createFromFormat($format, $dateStr);
   return $date && $date->format($format) === $dateStr;
}

function fxCleanString($p_cCadena) {
   $lcCadena = str_replace("Ñ", "N", $p_cCadena);
   $lcCadena = str_replace("Á", "A", $lcCadena);
   $lcCadena = str_replace("É", "E", $lcCadena);
   $lcCadena = str_replace("Í", "I", $lcCadena);
   $lcCadena = str_replace("Ó", "O", $lcCadena);
   $lcCadena = str_replace("Ú", "U", $lcCadena);
   $lcCadena = str_replace("á", "A", $lcCadena);
   $lcCadena = str_replace("é", "E", $lcCadena);
   $lcCadena = str_replace("í", "I", $lcCadena);
   $lcCadena = str_replace("ó", "O", $lcCadena);
   $lcCadena = str_replace("ú", "U", $lcCadena);
   $lcCadena = str_replace('"', "", $lcCadena);
   $lcCadena = str_replace("'", "", $lcCadena);
   return $lcCadena;
}

function fxInitSessionPTV2() {
   if (!(isset($_SESSION["GCNOMBRE"]) and isset($_SESSION["GCNRODNI"]))) {
      return false;
   }
   return true;
}

//Invoca Python
function fxInvocaPython($p_cCommand) {
   $lcData = shell_exec($p_cCommand);
   if (is_null($lcData)) {
      $lcData = '{"ERROR":"ERROR EN SHELL_EXEC"}';
   }
   $laArray = json_decode($lcData, true);
   if (is_null($laArray)) {
      $laArray = ["ERROR" => "ERROR EN JSON"];
   }
   return $laArray;
}

function fxCorrelativo($p_cCodigo) {
   $lcCodigo = $p_cCodigo;
   $i = strlen($p_cCodigo) - 1;
   while ($i >= 0) {
      $lcDigito = substr($p_cCodigo, $i, 1);
      if ($lcDigito == "9") {
         $lcDigito = "A";
      } elseif ($lcDigito < "9") {
         $lcDigito = strval(intval($lcDigito) + 1);
      } elseif ($lcDigito < "Z") {
         $lcDigito = chr(ord($lcDigito) + 1);
      } elseif ($lcDigito == "Z") {
         $lcDigito = "0";
      }
      $lcCodigo[$i] = $lcDigito;
      if ($lcDigito != "0") {
         break;
      }
      $i--;
   }
   return $lcCodigo;
}

function fxAlertNew($p_Message, $p_IdAlert, $p_nTimer = 2000) {
   if ($p_IdAlert === 1) {
      fxAlertExito($p_Message, $p_nTimer);
   } elseif ($p_IdAlert === 0) {
      fxAlertError($p_Message);
   } elseif ($p_IdAlert === 2) {
      fxAlertExitoAceptar($p_Message);
   } elseif ($p_IdAlert === 3) {
      fxAlertErrorAceptar($p_Message);
   }
}

function fxAlertExito($p_Message, $p_nTimer) {
   echo "<script type=\"text/javascript\">";
   echo "Swal.fire({";
   echo "icon: 'success',";
   echo "title: '$p_Message',";
   echo "showConfirmButton: false,";
   echo "timer: $p_nTimer";
   echo "})";
   echo "</script>";
}

function fxAlertExitoAceptar($p_Message) {
   echo "<script type=\"text/javascript\">";
   echo "Swal.fire({";
   echo "icon: 'success',";
   echo "title: '$p_Message',";
   echo "showConfirmButton: true";
   echo "})";
   echo "</script>";
}

function fxAlertError($p_Message) {
   echo "<script type=\"text/javascript\">";
   echo "Swal.fire({";
   echo "icon: 'error',";
   echo "title: '$p_Message',";
   echo "showConfirmButton: false,";
   echo "timer: 2000";
   echo "})";
   echo "</script>";
}

function fxAlertErrorAceptar($p_Message) {
   echo "<script type=\"text/javascript\">";
   echo "Swal.fire({";
   echo "icon: 'error',";
   echo "title: '$p_Message',";
   echo "showConfirmButton: true";
   echo "})";
   echo "</script>";
}
?>
