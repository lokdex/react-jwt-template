<?php

use Firebase\JWT\JWT;
use Firebase\JWT\Key;

require_once "Clases/CBase.php";
require_once "Clases/CSql.php";
require_once "Clases/jwt/JWT.php";
require_once "Clases/jwt/Key.php";

class CLogin extends CBase {
   public $paData, $paDatos;

   public function __construct() {
      parent::__construct();
      $this->paData = $this->paDatos = null;
   }

   public function omIniciarSesion() {
      $llOk = $this->mxValInicioSesion();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxIniciarSesion($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }

   protected function mxValInicioSesion() {
      if (
         !isset($this->paData["CNRODNI"]) ||
         (strlen(trim($this->paData["CNRODNI"])) != 8 &&
            (strlen(trim($this->paData["CNRODNI"])) < 7 ||
               strlen(trim($this->paData["CNRODNI"])) > 9))
      ) {
         $this->pcError = "NÚMERO DE DNI INVÁLIDO";
         return false;
      } elseif (
         !ctype_digit($this->paData["CNRODNI"]) &&
         strlen(trim($this->paData["CNRODNI"])) == 8
      ) {
         $this->pcError = "INGRESAR UN NÚMERO DE DNI VÁLIDO";
         return false;
      } elseif (
         !isset($this->paData["CCLAVE"]) ||
         strlen(trim($this->paData["CCLAVE"])) == 0
      ) {
         $this->pcError = "CONTRASEÑA INVÁLIDA";
         return false;
      }
      $this->paData["CCLAVE"] = hash("sha512", $this->paData["CCLAVE"]);
      return true;
   }

   protected function mxIniciarSesion($p_oSql) {
      $lcJson = json_encode($this->paData);
      $lcSql = "SELECT P_LOGIN('$lcJson')";
      $RS = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($RS);
      $laFila[0] = !$laFila[0]
         ? '{"ERROR": "ERROR DE EJECUCION DE BASE DE DATOS"}'
         : $laFila[0];
      $this->paDatos = json_decode($laFila[0], true);
      if (!empty($this->paDatos["ERROR"])) {
         $this->pcError = $this->paDatos["ERROR"];
         return false;
      }
      $jwt = new JWT();
      $key = "INGLORIOUS";
      $payload = [
         "exp" => time() + 300,
         "CCODUSU" => $this->paDatos["CCODUSU"],
      ];
      $token = $jwt->encode($payload, $key, "HS256");
      $this->paDatos["Access"] = $token;
      $payload = [
         "exp" => time() + 900,
         "CCODUSU" => $this->paDatos["CCODUSU"],
      ];
      $cookie = $jwt->encode($payload, $key, "HS256");
      setcookie("UCSM", $cookie, time() + 900, "/", "", true, true);
      return true;
   }

   public function omValidateToken() {
      $jwt = new JWT();
      // $key = new Key();
      $key = "INGLORIOUS";
      if (!isset($_SERVER["HTTP_AUTHORIZATION"])) {
         return false;
      }
      $auth = explode(" ", $_SERVER["HTTP_AUTHORIZATION"]);
      try {
         $token = $jwt->decode($auth[1], new Key($key, "HS256"));
      } catch (Exception $e) {
         return false;
      }
      return true;
   }

   public function omRefreshToken() {
      $jwt = new JWT();
      $key = "INGLORIOUS";
      $cookie = $_COOKIE["UCSM"];
      try {
         $token = $jwt->decode($cookie, new Key($key, "HS256"));
      } catch (Exception $e) {
         return false;
      }
      $claims = (array) $token;
      $payload = [
         "exp" => time() + 300,
         "CCODUSU" => $claims["CCODUSU"],
      ];
      $token = $jwt->encode($payload, $key, "HS256");
      $payload = [
         "exp" => time() + 900,
         "CCODUSU" => $claims["CCODUSU"],
      ];
      $cookie = $jwt->encode($payload, $key, "HS256");
      setcookie("UCSM", $cookie, time() + 900, "/", "", true, true);
      $this->paDatos = [
         "AccTkn" => $token,
         "CodUsu" => $claims["CCODUSU"],
      ];
      return true;
   }
}
?>
